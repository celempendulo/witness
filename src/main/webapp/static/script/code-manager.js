


function handleCode(data)
{
    if(!(data["TYPE"] === undefined))
    {
        switch (data["TYPE"])
        {
            case "SHOW OUTPUT":
                var text = $("#console").text();
                var first = data["FIRST"];
                console.log(first);
                if(first === true)
                {
                    text = "";
                }
                $("#console").text(text + data["CONTENT"]);
                break;
            
            case "ALLOW_INPUT":
               console.log("CONSOLE SHOULD BE EDITABLE");
            break;
            
            case "BLOCK_INPUT":
                console.log("CONSOLE SHOULD BE READ-ONLY");
            break;
                 
            default :
                console.log("UNKNONW TYPE : "+JSON.stringify(data));
                break;
        }
    }
    else
    {
        alert("TYPE IS MISSING");
    }
}

function loadFile()
{
    var reader = new FileReader();
    reader.onload = function(event)
    {
        var content = event.target.result;
        console.log(content);
    };
}


