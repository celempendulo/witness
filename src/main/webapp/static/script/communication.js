/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global ace */

var socket;
var editor = ace.edit("editor");
editor.session.setMode("ace/mode/java");

$(document).ready(function()
{
    socket = new WebSocket("ws://localhost:8080/endpoint");
    
    socket.onmessage = processMessage;

    socket.onopen = init;

    socket.onclose = function(){};

    socket.onerror = function(){};
    
    $("#run").click(function(){
        sendMessage();
    });
});


function init()
{
    var message = {"TYPE":"ADMIN","ACTION":"INIT"};
    socket.send(JSON.stringify(message));
}

function processMessage(event)
{
    var data = JSON.parse(event.data);
    if(data.TYPE === 'ADMIN')
    {
        handleAdminAction(data);
    }
    else if(data.TYPE === 'CODE')
    {
        handleCodeActions(data);
    }
    
}

function handleCodeActions(message)
{
    console.log(message.ACTION);
    if(message.ACTION === 'SHOW OUTPUT')
    {
        if(message.FIRST)
        {
            $("#console").text("");
        }
        
        $("#console").append(message.CONTENT);
    }
}


function handleAdminAction(message)
{
    if(message.ACTION === 'INIT')
    {
        var languages = JSON.parse(message.DATA);
        $('#languages').find('option').remove();
        var option;
        languages.forEach(function (language, index) 
        {
            option = new Option(language.TEXT);
            option.setAttribute("version",language.VERSION);
            option.setAttribute("name", language.NAME);
            option.setAttribute("extension", language.EXTENSION);
            $('#languages').append(option);
        });
    }
    else
    {
        console.log("message : "+message);
    }
}


function sendMessage()
{
    var filename = $("#filename").val().trim();
    var language = $('#languages').find(":selected");
    var sourceCode = editor.getValue();
            
    if(filename.length === 0)
    {
        alert("FILENAME CANNOT BE EMPTY");
    }
    var message = {
                    "FILENAME":filename, 
                    "FILE EXTENSION":language.attr("extension"), 
                    "LANGUAGE VERSION":language.attr("version"), 
                    "LANGUAGE NAME":language.attr("name"), 
                    "ACTION":"RUN", 
                    "TYPE": "CODE",
                    "SOURCE CODE":sourceCode
                };
                
    var action = {"TYPE":"CODE", "ACTION":"RUN", "MESSAGE":message};
    socket.send(JSON.stringify(action));
    
    action = {"TYPE":"CODE", "ACTION":"ADD INPUT", "INPUT":" python is cool"};
    socket.send(JSON.stringify(action));
}

