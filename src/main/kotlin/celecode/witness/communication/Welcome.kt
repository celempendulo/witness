package celecode.witness.communication

import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

//@WebServlet(name = "welcome", value = ["/"])
class Welcome : HttpServlet() {

    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse)
    {
       // resp.sendRedirect("index.html");
    }

    override fun doPost(req: HttpServletRequest, resp: HttpServletResponse) {
        doGet(req, resp)
    }
}