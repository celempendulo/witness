/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.ide.pipeline.component;

import celecode.ide.configuration.Language;
import celecode.ide.pipeline.ICommunicator;
import celecode.ide.pipeline.Message;
import celecode.ide.pipeline.Message.Field;
import celecode.ide.util.Constants;
import celecode.ide.util.Constants.Property;
import celecode.ide.util.MessageUtils;
import celecode.ide.util.ParamUtils;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 *
 * @author mpendulo
 */
public class PipelineComponentFileManager extends APipelineComponent
{
    private Path codePath;
    private Path inputPath;

    @Override
    public void init(List<Language> languages, String params) throws Exception 
    {
        
        ParamUtils paramUtils = new ParamUtils(params);
        codePath = Paths.get(paramUtils.getString(Params.CODE_PATH,
                Params.DEFAULT_CODE_PATH).replace(
                Constants.PlaceHolder.HOME, System.getProperty(Property.HOME)));
        
        inputPath = Paths.get(paramUtils.getString(Params.INPUT_PATH,
                Params.DEFAULT_INPUT_PATH).replace(
                Constants.PlaceHolder.HOME, System.getProperty(Property.HOME)));
        
        if(!Files.isDirectory(codePath))
        {
            Files.createDirectories(codePath);
        }
       
        if(!Files.isDirectory(inputPath))
        {
            Files.createDirectories(inputPath);
        }
        
        
    }

    @Override
    public void process(Message message, ICommunicator communicator)
            throws Exception 
    {
        
       if(MessageUtils.isPresent(message, 
               new Message.Field[]{
                   Message.Field.FILENAME, 
                   Message.Field.FILE_EXTENSION,
                   Message.Field.SOURCE_CODE}))
       {

        String filename = message.getField(Message.Field.FILENAME);
        String fileExtension = message.getField(Message.Field.FILE_EXTENSION);
        String sourceCode = message.getField(Message.Field.SOURCE_CODE);
        String folderName = getRandomFolderName();

        // Generate user folder, create it and add it to the message for later use
        Path userFolder = codePath.resolve(folderName);
        Files.createDirectory(userFolder);
        message.addField(Message.Field.USER_FOLDER, 
        		userFolder.toAbsolutePath().toString());

        // Create file and add content to it
        Path file = userFolder.resolve(filename+fileExtension);
        Files.createFile(file);
        Files.write(file, sourceCode.getBytes());
        
        Path inputFile = inputPath.resolve(folderName+".txt");
        Files.createFile(inputFile);
        message.addField(Field.INPUT_FILE, inputFile.toAbsolutePath().toString());
        
       }
       
       
    }
    
    private String getRandomFolderName()
    {
    
        return new Timestamp(new Date().getTime()).toString();
    }

    
    public static class Params
    {
        public static final String CODE_PATH = "CODE_PATH";
        public static final String INPUT_PATH = "INPUT_PATH";
        private static final String DEFAULT_CODE_PATH = "{home}/witness/code";
        private static final String DEFAULT_INPUT_PATH = "{home}/witness/input";
    }
    
}
