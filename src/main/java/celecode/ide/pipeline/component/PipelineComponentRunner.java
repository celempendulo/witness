/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.ide.pipeline.component;


import celecode.ide.configuration.Language;
import celecode.ide.pipeline.ICommunicator;
import celecode.ide.pipeline.Message;
import celecode.ide.util.Constants;
import celecode.ide.util.MessageUtils;
import java.io.File;
import java.util.List;

/**
 *
 * @author mpendulo
 */
public class PipelineComponentRunner extends APipelineComponent
{

    private List<Language> languages;

    @Override
    public void init(List<Language> languages, String params) throws Exception 
    {
        if(languages == null || languages.isEmpty())
        {
            throw  new Exception("LANGUAGES CANNOT BE NULL OR EMPTY");
        }
        this.languages = languages;
    }

    @Override
    public void process(Message message, ICommunicator communicator) 
            throws Exception 
    {
        if(MessageUtils.isPresent(message, 
            new Message.Field[]{
                Message.Field.COMMAND, 
                Message.Field.FILENAME, 
                Message.Field.FILE_EXTENSION,
                Message.Field.LANGUAGE_NAME, 
                Message.Field.LANGUAGE_VERSION, 
                Message.Field.USER_FOLDER
        }))
        {
        
            if(message.isFieldSet(Message.Field.COMMAND) && 
              Message.Command.RUN.equals(message.getField(
                      Message.Field.COMMAND)))
            {
                ensureFilesExist(message);

                Language language;
                String languageName = message.getField(
                        Message.Field.LANGUAGE_NAME);
                String languageVersion = message.getField(
                        Message.Field.LANGUAGE_VERSION);
                if((language = getSupportedLanguage(languages, 
                        languageName, Double.parseDouble(languageVersion)))
                        == null)
                {
                    throw new Exception("LANGUAGE "+ languageName+", "
                            + "VERSION "+languageVersion+" IS NOT SUPPORTED");
                }

                if(language != null)
                {
                    run(language, message, communicator);
                } 
                
            }
        }
    }

    
    private void run(Language language, Message message, 
            ICommunicator communicator) throws Exception
    {
        String filename = message.getField(Message.Field.FILENAME);
        String fileExtension = message.getField(Message.Field.FILE_EXTENSION);
        String userFolder = message.getField(Message.Field.USER_FOLDER);
        
        String runCommand = language.getRunCommand();
        runCommand = runCommand.replace(Constants.PlaceHolder.FILENAME, 
                filename);
        runCommand = runCommand.replace(Constants.PlaceHolder.EXTENSION,
                fileExtension);
        
        ProcessBuilder builder = new ProcessBuilder(runCommand.split(" "));
        builder = builder.directory(new File(userFolder));
        Process process = builder.start();


        communicator.setOutputStream(process.getOutputStream());
        communicator.enableInteraction();
        
        inheritIO(process.getInputStream(), communicator);
        inheritIO(process.getErrorStream(), communicator);
        
        communicator.disableInteraction();
        //communicator.sendCodeCommand(Action.CodeActions.BLOCK_INPUT, null);
    }
    
}
