/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.ide.pipeline.component;

import celecode.ide.configuration.Language;
import celecode.ide.pipeline.ICommunicator;
import celecode.ide.pipeline.Message;
import celecode.ide.util.MessageUtils;
import java.util.List;

/**
 *
 * @author mpendulo
 */
public class PipelineComponentQuestion extends APipelineComponent
{


    @Override
    public void init(List<Language> languages, String params) throws Exception 
    {
    }

    @Override
    public void process(Message message, ICommunicator communicator) 
            throws Exception 
    {
        MessageUtils.isPresent(message, 
            new Message.Field[]{
                Message.Field.COMMAND, 
                Message.Field.FILENAME, 
                Message.Field.FILE_EXTENSION,
                Message.Field.LANGUAGE_NAME, 
                Message.Field.LANGUAGE_VERSION, 
                Message.Field.SOURCE_CODE,
                Message.Field.USER_FOLDER
            });
    }
    
}
