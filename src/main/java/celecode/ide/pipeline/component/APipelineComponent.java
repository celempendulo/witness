/*
 * Copyright 2019 mpendulo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package celecode.ide.pipeline.component;

import celecode.ide.configuration.Language;
import celecode.ide.pipeline.ICommunicator;
import celecode.ide.pipeline.Message;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

/**
 * Classes that extend this class are to be used to process IDE source
 * code related requests such as running files/script
 * 
 * 
 * @author mpendulo
 */
public abstract class APipelineComponent 
{

    public APipelineComponent() 
    {
        
    }
    
    /**
     * This method is used to initialize a pipeline component
     * 
     * @param languages a list of all supported programming languages
     * 
     * @param params pipeline component specific parameters
     * 
     * @throws Exception 
     */
    public abstract void init(List<Language> languages, String params) 
            throws Exception;
    
    /**
     * 
     * @param message
     * @param communicator
     * @throws Exception 
     */
    public abstract void process(Message message, ICommunicator communicator)
            throws Exception;
    
    
    protected String replaceFirst(String mainString, String subString, String replacement)
    {
        if(mainString == null || subString == null)
        {
            return mainString;
        }
        mainString = mainString.replace(subString, replacement);
        
        return mainString;
    }
    
    protected void inheritIO(final InputStream src, 
            final ICommunicator communicator) 
    {
        try(Scanner scanner = new Scanner(src))
        {
            while (scanner.hasNextLine())
            {
                String line = scanner.nextLine();
                communicator.sendConsoleOutput(line +" \n");
            }
        }
    }
    
    protected boolean isLanguageSupported(List<Language> languages, String languageName, double languageVersion)
    {
        return getSupportedLanguage(languages, languageName, languageVersion) != null;
    }
    
    protected Language getSupportedLanguage(List<Language> languages, String languageName, double languageVersion)
    {
        Language supportedLanguage = null;
        for(Language language: languages)
        {
            if(language.getName().equalsIgnoreCase(languageName) && 
               languageVersion == language.getVersion())
            {
                supportedLanguage = language;
                break;
            }
        }
        
        return supportedLanguage;
    
    }
    
    protected boolean isDirectory(String path)
    {
        return Files.isDirectory(Paths.get(path));
    }
    
    protected boolean isFile(String path)
    {
        return Files.isRegularFile(Paths.get(path));
    }
    
    protected void ensureFilesExist(Message message) throws Exception
    {
        if(!isDirectory(message.getField(Message.Field.USER_FOLDER)))
        {
            throw new Exception("USER FOLDER "+message.getField(Message.Field.USER_FOLDER)+" DOES NOT EXIST OR"
                    + " IS NOT A FOLDER");
        }
        
        Path path = Paths.get(message.getField(Message.Field.USER_FOLDER));
        path = path.resolve(message.getField(Message.Field.FILENAME)+message.getField(Message.Field.FILE_EXTENSION));
        if(!isFile(path.toAbsolutePath().toString()))
        {
            throw new Exception(path.toAbsolutePath().toString()+" DOEST NOT EXIST");
        }
    }
}
