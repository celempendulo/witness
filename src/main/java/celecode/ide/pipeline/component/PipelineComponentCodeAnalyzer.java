/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.ide.pipeline.component;

import celecode.ide.configuration.Language;
import celecode.ide.pipeline.ICommunicator;
import celecode.ide.pipeline.Message;
import celecode.ide.util.MessageUtils;
import java.util.List;

/**
 *
 * @author mpendulo
 */
public class PipelineComponentCodeAnalyzer extends APipelineComponent
{     
    private List<Language> languages;
    
    @Override
    public void init(List<Language> languages, String params) throws Exception
    {
        if(languages == null || languages.isEmpty())
        {
            throw  new Exception("LANGUAGES CANNOT BE NULL OR EMPTY");
        }
        this.languages = languages;  
    }

    @Override
    public void process(Message message, ICommunicator communicator) 
            throws Exception 
    {
        if(MessageUtils.isPresent(message, 
                new Message.Field[]{
                    Message.Field.COMMAND, 
                    Message.Field.FILENAME, 
                    Message.Field.FILE_EXTENSION,
                    Message.Field.LANGUAGE_NAME, 
                    Message.Field.LANGUAGE_VERSION, 
                    Message.Field.SOURCE_CODE,
                    Message.Field.USER_FOLDER
        }))
        {
            String languageName = message.getField(Message.Field.
                    LANGUAGE_NAME).trim();
            String languageVersion = message.getField(Message.Field.
                    LANGUAGE_VERSION);
            if(!isLanguageSupported(languages, languageName, 
                    Double.parseDouble(languageVersion)))
            {
                throw new Exception("LANGUAGE "+ languageName+", VERSION "+
                        languageVersion+" IS NOT SUPPORTED");
            }

            ensureFilesExist(message);

            if(Message.Command.ANSWER_QUESTION.equals(message.
                    getField(Message.Field.COMMAND)))
            {

                MessageUtils.isPresent(message, 
                        new Message.Field[]{
                            Message.Field.QUESTION,
                            Message.Field.QUESTION_OWNER
                });

            }
        }
    }
    
    public static final String JAVA = "JAVA";
    
}
