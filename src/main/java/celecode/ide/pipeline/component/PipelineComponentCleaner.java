/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.ide.pipeline.component;

import celecode.ide.configuration.Language;
import celecode.ide.pipeline.ICommunicator;
import celecode.ide.pipeline.Message;
import celecode.ide.util.MessageUtils;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mpendulo
 */
public class PipelineComponentCleaner extends APipelineComponent
{

    @Override
    public void init(List<Language> languages, String params) throws Exception 
    {

    }

    @Override
    public void process(Message message, ICommunicator communicator) 
            throws Exception 
    {
        if(MessageUtils.isPresent(message, new Message.Field[]{Message.
                Field.COMMAND}))
        {
            if(message.isFieldSet(Message.Field.USER_FOLDER))
            {
                delete(Paths.get(message.getField(Message.Field.USER_FOLDER)));
            }
            
            if(message.isFieldSet(Message.Field.INPUT_FILE))
            {
                delete(Paths.get(message.getField(Message.Field.INPUT_FILE)));
            }
        }
    }
    
    public static void delete(Path directory)
    {
        if(Files.isDirectory(directory)) 
        {
            try
            {
                Files.walkFileTree(directory, new FileDeleteVisitor());
            }
            catch (IOException ex)
            {
                Logger.getLogger(PipelineComponentCleaner.class.
                        getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static class FileDeleteVisitor extends SimpleFileVisitor<Path>
    {

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                throws IOException
        {
            Files.delete(file);
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc)
                throws IOException 
        {
            Files.delete(dir);
            return FileVisitResult.CONTINUE;
        }
           
    }
    
    public static class Params
    {
        public static final String DELAY = "DELAY";
    }
}
