/*
 * Copyright 2019 mpendulo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package celecode.ide.pipeline;

import celecode.ide.configuration.Configuration;
import celecode.ide.configuration.Component;
import celecode.ide.configuration.Language;
import celecode.ide.pipeline.component.APipelineComponent;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *This class is the center of the IDE,
 * 
 * All request are processed by this class with the use of pipeline
 * components
 * 
 * @author mpendulo
 */
public class Engine 
{
    
    private final List<APipelineComponent> pipelineComponents;
    private boolean initialised;

    public Engine() 
    {
        initialised = false;
        pipelineComponents = new LinkedList<>();
    }
    
    /**
     * Initialize pipeline the pipeline engine with configuration data,
     * If the engine fails to initialize the IDE application should 
     * terminate as no request can be processed
     * 
     * @param configuration
     * 
     * @throws Exception 
     */
    public void init(Configuration configuration) throws Exception
    {
    	if(initialised)
    	{
    		throw new Exception("PIPELINE ENGINE IS ALREADY INITIALIZED");
    	}
        List<Component> components = configuration.getComponents();
        List<Language> languages = configuration.getLanguages();
        
        if(components == null || languages == null)
        {
            throw new Exception("INVALID CONFIGURATION, COMPONENTS OR LANGUAGES IS NULL");
        }
        
        languages.stream().filter((language)-> !language.isValid());
        components.stream().filter((component)-> !component.isValid());
        Collections.sort(configuration.getComponents());
        
        if(languages.isEmpty())
        {
        	throw new Exception("NO VALID PROGRAMMING LANGUAGE IS CONFIGURED");
        }
        
        if(components.isEmpty())
        {
        	throw new Exception("NO VALID PIPELINE COMPONENT IS CONFIGURED");
        }
        
        List<Exception> errorMessages = new LinkedList<>();
        components.stream().forEach((Component component)->
        {
        	
            try
            {
                if(component.isActive())
                {
                    APipelineComponent comp = (APipelineComponent) Class.
                            forName(component.getName()).getDeclaredConstructor().newInstance();
                    comp.init(languages, component.getParams());
                    pipelineComponents.add(comp);
                }
            }
            catch(Exception exception)
            {
                
                
                if(component.isCompulsory())
                {
                    initialised = false;
                    errorMessages.add(
                    new Exception("CLASS "+String.valueOf(component.getName())+
                            " FAILED WITH MESSAGE "+ exception.getMessage()));
                }
            }
        });
        
        if(!errorMessages.isEmpty())
        {
        	// Log error message
        	throw errorMessages.get(0);
        }
        
        initialised = true;
    }
    
    
    /**
     * This method is used to process a message request from IDE client/
     * extension.
     * 
     * @param message containing data about the request
     * @param communicator to communicate with the client/extension while the
     *         request is being processed
     * @throws Exception
     * 
     * 
     */
    public void process(Message message, ICommunicator communicator) 
            throws Exception
    {
        
        if(!initialised)
        {
            throw new Exception("ENGINE DIDN'T INITIALIZE");
        }
        
        if(message == null)
        {
            //throw new Exception("NULL MESSAGE CANNOT BE PROCESSED");
        }
        
        new Thread(()->
        {
            pipelineComponents.forEach((component) -> 
            {
                try
                {
                    component.process(message, communicator);
                }
                catch (Exception ex)
                {
                    Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        }).start();

    }

    /**
     * Gets a flag indicating if the the pipeline engined is initialized 
     * or not
     * 
     * @return 
     */
    public boolean isInitialised() 
    {
        return initialised;
    }
    
    
    
    
}
