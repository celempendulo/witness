/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.ide.pipeline;

import java.io.OutputStream;

/**
 *
 * @author mpendulo
 */
public interface ICommunicator 
{
    /**
     * This method is used to send console output to the client
     * 
     * @param codeOutput  output string to be displayed on the console
     */
    public void sendConsoleOutput(String codeOutput);
    
    
    /**
     * This method is used to supply input to the running application
     * 
     * @param codeInput 
     */
    public void writeCodeInput(String codeInput);
    
    
    /**
     * This method is use to enable interaction with the client.<br>
     * Normally this method should send a notification to the client.<br>
     * This method should be called by a pipeline component that is running the 
     * script.
     */
    public void enableInteraction();
    
    
    
    /**
     * This method is use to disable interaction with the client.<br>
     * Normally this method should send a notification to the client.<br>
     * This method should be called by a pipeline component that is running the 
     * script (when it has finished).
     */
    public void disableInteraction();
    
    
    /**
     * This method sets stream to be used to write input during interaction.
     * 
     * @param stream 
     */
    public void setOutputStream(OutputStream stream);
}
