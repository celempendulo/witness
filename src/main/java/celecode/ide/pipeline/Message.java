/*
 * Copyright 2019 mpendulo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package celecode.ide.pipeline;

import java.util.Map;
import java.util.TreeMap;

/**
 *
 * This class represent a source code message (request),
 * This message contain fields required by different pipeline components
 * to process the request
 * 
 * 
 * @author mpendulo
 */
public class Message 
{
    
    private static final Map<String, Field> FIELDS = new TreeMap<>();
    private final Map<Field,String> fields;
    
    public Message()
    {
        fields = new TreeMap<>();
    }

    public Message(Map<Field, String> fields) 
    {
        if(fields != null)
        {
            this.fields = new TreeMap<>();
        }
        else
        {
            this.fields = fields;
        }
    }
    
    /**
     * Checks if a specific field is set
     * 
     * @param field
     * @return <code>true</code> if field is set <br>
     *           otherwise <code>false</code>
     */
    public boolean isFieldSet(Field field)
    {
        return fields.keySet().contains(field);
    }
    
    /**
     * Gets a specific field
     * 
     * @param field required field
     * @return String representation of field if field is set <br>
     *         otherwise returns <code>false</code>
     */
    public String getField(Field field)
    {
        return fields.get(field);
    }
    
    public void addField(Field field, String value) throws Exception
    {
        if(value == null)
        {
            fields.put(field, null);
            return;
        }
        
        if(field.getType() == DataType.DOUBLE)
        {
           Double.parseDouble(value);
        }
        
        if(field.getType() == DataType.INTEGER)
        {
            Integer.parseInt(value);
        }
        
        if(field.getType() == DataType.LONG)
        {
            Long.parseLong(value);
        }
        
        if(field.length != Length.VAR)
        {
            if(field.length != value.length())
            {
                throw  new Exception("INVALID LENGTH FOR FIELD "+field.getName());
            }
        }
        
        fields.put(field, value);
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        fields.entrySet().stream().map((entry) -> 
        {   
            builder.append(entry.getKey().getName()).append("\t = ").append(String.valueOf(entry.getValue()));
            return entry;
        }).forEachOrdered((_item) -> 
        {
            builder.append("\n");
        });
        return builder.toString();
    }
    
   
    public static enum Field
    {
        SOURCE_CODE("SOURCE CODE",Length.VAR, DataType.STRING),
        FILENAME("FILENAME",Length.VAR, DataType.STRING),
        FILE_EXTENSION("FILE EXTENSION", Length.VAR, DataType.STRING),
        LANGUAGE_VERSION("LANGUAGE VERSION", Length.VAR, DataType.DOUBLE),
        LANGUAGE_NAME("LANGUAGE NAME", Length.VAR, DataType.STRING),
        COMMAND("ACTION", Length.VAR, DataType.STRING),
        ERROR("ERROR CODE", Length.THREE, DataType.STRING),
        ERROR_DESCRIPTION("ERROR DESCRIPTION", Length.VAR, DataType.STRING),
        USER_FOLDER("USER FOLDER", Length.VAR, DataType.STRING),
        QUESTION("QUESTION",Length.VAR, DataType.LONG),
        QUESTION_OWNER("QUESTION OWNER", Length.VAR, DataType.LONG),
        INPUT_FILE("INPUT_FILE", Length.VAR, DataType.STRING);
        
        private final String name;
        private final int type;
        private final int length;
        
        private Field(String name, int length, int type)
        {
            this.name = name;
            this.length = length;
            this.type = type;
        }

        public String getName() 
        {
            return name;
        }

        public int getLength() 
        {
            return length;
        }

        public int getType()
        {
            return type;
        }
          
    }
    
    public static class Command
    {
        public static final String RUN = "RUN";
        public static final String COMPILE = "COMPILE";
        public static final String SAVE = "SAVE";
        public static final String ANSWER_QUESTION = "ANSWER_QUESTION";
        public static final String ANALYSE = "ANALYSE";
    }
    
    /**
     * This class is used to store a set of data lengths supported by source 
     * code request message
     */
    public static final class Length
    {
        public static final int VAR =  -1;
        public static final int ONE = 1;
        public static final int TWO = 2;
        public static final int THREE = 3;
        
    }
    
    /**
     * This is used to represent a set data types that are supported by the 
     * source code request message
     */
    public static final class DataType
    {
        public static final int INTEGER = 1;
        public static final int BOOLEAN = 2;
        public static final int DOUBLE = 3;
        public static final int STRING = 4;
        public static final int LONG = 5;
    }
    
    
    static
    {
        FIELDS.put(Field.COMMAND.getName(), Field.COMMAND);
        FIELDS.put(Field.ERROR.getName(), Field.ERROR);
        FIELDS.put(Field.ERROR_DESCRIPTION.getName(),
                Field.ERROR_DESCRIPTION);
        FIELDS.put(Field.FILENAME.getName(), Field.FILENAME);
        FIELDS.put(Field.FILE_EXTENSION.getName(), 
                Field.FILE_EXTENSION);
        FIELDS.put(Field.LANGUAGE_NAME.getName(), Field.LANGUAGE_NAME);
        FIELDS.put(Field.LANGUAGE_VERSION.getName(), 
                Field.LANGUAGE_VERSION);
        FIELDS.put(Field.SOURCE_CODE.getName(), Field.SOURCE_CODE);
        FIELDS.put(Field.USER_FOLDER.getName(), Field.USER_FOLDER);
        FIELDS.put(Field.QUESTION.getName(), Field.QUESTION);
        FIELDS.put(Field.INPUT_FILE.getName(), Field.INPUT_FILE);
    }
    
    /**
     * Gets a field enum by name
     * @param fieldName name of field required
     * @return 
     */
    public static Field getField(String fieldName)
    {
        return FIELDS.get(fieldName);
    }
}
