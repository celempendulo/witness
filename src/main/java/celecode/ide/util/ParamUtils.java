/*
 * Copyright 2019 mpendulo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package celecode.ide.util;

import java.util.Map;
import java.util.TreeMap;

/**
 *
 * This class is used to manipulate pipeline component parameters
 * 
 * 
 * @author mpendulo
 */
public class ParamUtils 
{
    
   public static final String SEPARATOR = ",";
   public static final String EQUAL = "=";
   private final Map<String,String> values;

    public ParamUtils(String params) 
    {
        values = new TreeMap<>();
        addValues(params);
    }
    
    private void addValues(String params)
    {
        if(params != null)
        {
            String[] splitted = params.split(SEPARATOR);
            for(String pair : splitted)
            {
                int index;
                if((index = pair.indexOf(EQUAL)) != -1)
                {
                    if(index != 0)
                    {
                        String key = pair.substring(0, index);
                        if(index != pair.length()-1)
                        {
                            String value = pair.substring(index+1);
                            if(!(key = key.trim()).isEmpty())
                            {
                                values.put(key, value);
                            }
                        }
                    }
                }
            }
        }
    }
   
    
    public String getString(String key) throws Exception
    {
        String val = getString(key, null);
        if(val == null)
        {
            throw  new Exception(key+ " IS MISSING");
        }
        
        return val;
    }
    
    public String getString(String key, String value)
    {
        String val = values.get(key);
        if(val == null)
        {
            val = value;
        }
        
        return val;
    }
    
    public long getLong(String key, long value)
    {
        long val;
        try
        {
            val =  getLong(key);
        }
        catch(Exception ex)
        {
            val = value;
        }
        return val;
    }
    
    public long getLong(String key) throws Exception
    {
        String val = getString(key, null);
        if(val == null)
        {
            throw  new Exception(key+ " IS MISSING");
        }
        return Long.parseLong(val);
    }
    
}
