/*
 * Copyright 2019 mpendulo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package celecode.ide.util;

import celecode.ide.pipeline.Message;

/**
 *This class contains utility methods from processing source code
 * related request messages
 * 
 * @author mpendulo
 */
public class MessageUtils 
{
    
    /**
     * Used to check if all required fields are present in a message
     * 
     * @param message
     * @param fields 
     * 
     * @return <code> true </code> if all required fields are present 
     *          <br> otherwise return <code>false</code>
     */
    public static boolean isPresent(Message message, Message.Field[] fields)
    {
        for(Message.Field field : fields)
        {
            if(!message.isFieldSet(field))
            {
                return false;
            }
        }
        return true;
    }
    
    /**
     *  Used to get missing field in a message 
     * 
     * @param message
     * @param fields
     * @return First field in fields that is absent in message 
     *          <br> If all fields are present a <code>null</code> value will
     *          be returned
     */
    
    public static Message.Field getMissingField(Message message,
            Message.Field[] fields)
    {
        
        for(Message.Field field : fields)
        {
            if(!message.isFieldSet(field))
            {
                return field;
            }
        }
        return null;
    }
    
}
