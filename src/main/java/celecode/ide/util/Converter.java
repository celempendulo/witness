package celecode.ide.util;

import celecode.ide.pipeline.Message;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.Map;

/**
 *This class contains utility methods to convert from various data types
 * 
 * @author mpendulo
 */
public class Converter 
{
    
	public static final ObjectMapper MAPPER = new ObjectMapper(); 
	
    /**
     * This method is used to convert a java object into a JSON string
     * 
     * @param object to be converted to a JSON string
     * 
     * @return JSON representation of object
     * 
     * @throws Exception should conversion fail
     */
    public static String fromObjectJson(Object object) throws Exception
    {
        return MAPPER.writeValueAsString(object);
    }
    
    /**
     * 
     * This method is used to convert a JSON string into a java object 
     * of specified class type
     * 
     * @param value JSON representation of object
     * 
     * @param type class of object
     * 
     * @return instance of type represented by value
     * 
     * @throws Exception should conversion fail
     */
    public static Object fromJsonToObject(String value, Class<?> type) 
            throws Exception
    {
        MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return MAPPER.readValue(value, type);
    }
    
    
    /**
     * This is a utility method used to convert a JSON string into a
     * source code request message
     * 
     * @param messageJson
     * 
     * @return instance of Message 
     * 
     * @throws Exception should conversion fail
     */
    public static Message fromJsonToMessage(String messageJson) throws Exception
    {
        if(messageJson == null)
        {
            throw new Exception("NULL MESSAGE CAN NOT BE CONVERTED");
        }
        Message message = new Message();
        Map<String, String> map = MAPPER.readValue(messageJson, 
                new TypeReference<Map<String, String>>(){});
        Message.Field field;
        for(Map.Entry<String,String> entry : map.entrySet())
        {
           field = Message.getField(entry.getKey().trim());
           if(field != null)
           {
               message.addField(field, entry.getValue());
           }
        }
        return message;
    }
    
    /**
     * This method is used to covert a map into a JSON string
     * 
     * @param map
     * @return 
     */
    public static String fromKeyValueArrayToString(Map<String, String[]> map)
    {
        ObjectNode node = MAPPER.createObjectNode();
        map.entrySet().forEach((entry) -> 
        {
            node.put(entry.getKey(), entry.getValue()[0]);
        });
        return node.toString();
    }
    
    /**
     * This method is used to convert a JSON string into a JSON
     * node object
     * 
     * @param nodeString
     * 
     * @return
     * @throws Exception 
     */
    public static JsonNode fromStringToObjectNode(String nodeString)
            throws Exception
    {
        return MAPPER.readTree(nodeString);
    }
}
