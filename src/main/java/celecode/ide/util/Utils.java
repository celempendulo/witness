package celecode.ide.util;

public class Utils
{
	
	
	
	public static boolean isLinux()
	{
		String os = System.getProperty("os.name");
		return (os.contains("nix") || os.contains("nux") || os.contains("aix"));
	}
	
    public static boolean isWindows() 
    {
    	String os = System.getProperty("os.name");
        return os.contains("win");
    }

}
