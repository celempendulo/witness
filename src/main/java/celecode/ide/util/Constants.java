/*
 * Copyright 2019 mpendulo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package celecode.ide.util;


/**
 * This class contains constants to be used by the IDE application
 * 
 * @author mpendulo
 */
public class Constants 
{
    
    
    /**
     * This class contains place holders that are used as part of the 
     * configuration e.g file name place holder that would be used when running 
     * a script.
     */
    public static class PlaceHolder
    {
        public static final String HOME = "{home}";
        public static final String FILENAME = "{filename}";
        public static final String EXTENSION = "{extension}";
        public static final String FOLDER = "{folder}";
    }
    
    /**
     * This class contains a list of java system properties constants
     * e.g key used to get system home directory ("user.home").
     */
    public static class  Property
    {
        public static final String HOME = "user.home";
    }
    
    /**
     * This class contains a list of system commands 
     * e.g a command to navigate to a folder.
     */
    public static class Command
    {
        public static final String NAVIGATE_TO_FOLDER = "cd "+PlaceHolder.FOLDER;
    }
    
}
