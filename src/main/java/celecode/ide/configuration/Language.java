/*
 * Copyright 2019 mpendulo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package celecode.ide.configuration;

/**
 * This class is used to represent a configured programming language
 * 
 * All information required about a specific programming language should
 * be found here
 * 
 * @author mpendulo
 */
public class Language 
{
    private String name;
        
    private double version;

    private boolean compilable;

    private String compileCommand;

    private String runCommand;

    private String extension;

    public Language() 
    {

    }

    /**
     * Sets programming language name, e.g java
     * 
     * @param name 
     * 
     */
    public void setName(String name) 
    {
        this.name = name;
    }

    /**
     * Sets programming language version, e.g 8.0 
     * 
     * @param version 
     * 
     */
    public void setVersion(double version)
    {
        this.version = version;
    }

    /**
     * Flag to indicate is programming language is compiled or not
     * 
     * @param compilable 
     */
    public void setCompilable(boolean compilable) 
    {
        this.compilable = compilable;
    }

    /**
     * Sets compile command for compilable language
     * 
     * e.g javac {filename}{extension} for java
     * 
     * @param compileCommand 
     */
    public void setCompileCommand(String compileCommand) 
    {
        this.compileCommand = compileCommand;
    }

    /**
     * Sets run command for a programming language, this is the 
     * command used run a file/script for with code written in this 
     * programming language
     * 
     * e.g java {filename} for java
     * 
     * @param runCommand 
     */
    public void setRunCommand(String runCommand) 
    {
        this.runCommand = runCommand;
    }

    /**
     * Sets an file extension for file with code written in this 
     * programming language
     * 
     * e.g .java for java
     * 
     * @param extension 
     */
    public void setExtension(String extension) 
    {
        this.extension = extension;
    }

    /**
     * Gets name of the programming language
     * 
     * @return name
     *          
     */
    public String getName() 
    {
        return name;
    }

    /**
     * Gets version of the programming language
     * 
     * @return version
     */
    public double getVersion() 
    {
        return version;
    }

    /**
     * 
     * Gets a flag indicating whether the programming language is 
     * compiled or not
     * 
     * @return <code>true</code> if the language is compilable <br>
     *          <code>false</code> otherwise
     */
    public boolean isCompilable() 
    {
        return compilable;
    }

    /**
     * Gets compile command for the programming language
     * 
     * @return compile command
     *         
     */
    public String getCompileCommand() 
    {
        return compileCommand;
    }
    
    /**
     * Gets run command fro the programming language
     * 
     * @return runCommand
     */
    public String getRunCommand() 
    {
        return runCommand;
    }

    /**
     * Get file extension for script/files containing code written in
     * this programming language 
     * 
     * @return file extension for the language
     */
    public String getExtension() 
    {
        return extension;
    }
    
    /**
     * Checks if the language configuration is valid
     * 
     * @return <code>true</code> if language configuration is valid <br>
     * 			otherwise <code>false</code>
     */
    public boolean isValid()
    {
    	if(compilable && (compileCommand == null || compileCommand.trim().isEmpty()))
    	{
            return false;
    	}
    	
    	if(name == null || name.trim().isEmpty() || runCommand == null ||
                runCommand.trim().isEmpty() || extension == null ||
                extension.trim().isEmpty() || extension.indexOf('.') == -1)
    	{
            return false;
    	}
    	
    	
    	return true;
    }

    @Override
    public String toString() 
    {
        return name+" "+version;
    }
        
        
}
