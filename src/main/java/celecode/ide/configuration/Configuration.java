/*
 * Copyright 2019 mpendulo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package celecode.ide.configuration;

import java.util.List;

/**
 * This class is used to represent IDE configuration
 * i.e supported programming languages languages, configured pipeline
 * components etc
 *
 * @author mpendulo
 */

public class Configuration 
{
    
   private List<Language> languages;
   
   private List<Component> components;

    public Configuration() 
    {
        
    }

    /**
     * Sets a list of configured pipeline components
     * 
     * @param components 
     */
    public void setComponents(List<Component> components)
    {
        this.components = components;
    }

    /**
     * Sets a list of supported programming lnguages
     * 
     * @param languages 
     */
    public void setLanguages(List<Language> languages) 
    {
        this.languages = languages;
    }

    /**
     * Gets a list of configured pipeline components
     * @return 
     */
    public List<Component> getComponents()
    {
        return components;
    }

    /**
     * Gets a list of supported programming language
     * 
     * @return 
     */
    public List<Language> getLanguages() 
    {
        return languages;
    }
}
