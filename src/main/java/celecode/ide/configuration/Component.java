/*
 * Copyright 2019 mpendulo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package celecode.ide.configuration;


/**
 * This class is used to represent a configured pipeline component
 * All information required about a specific configuration of a pipeline
 * component should be found here
 * 
 * <br>
 * 
 * This class implements Comparable interface and Components aree
 * compared by their priority
 * 
 * @author mpendulo
 */
public class Component implements Comparable<Component>
{
    private String name;

    private int priority;

    private String params;

    private boolean compulsory;

    private boolean active;

    public Component() 
    {

    }

    /**
     * Sets a flag indicating whether this pipeline component should be 
     * active or not, if the pipeline component is marked as in-active it
     * should not be used by the pipeline engine to process requests
     * 
     * @param active 
     */
    public void setActive(boolean active)
    {
        this.active = active;
    }

    /**
     * Sets a flag indicating whether this pipeline component is 
     * compulsory or not, If a compulsory pipeline component fails to 
     * initialize the IDE should fail to initialize.
     * 
     * @param compulsory 
     */
    public void setCompulsory(boolean compulsory) 
    {
        this.compulsory = compulsory;
    }
    
    /**
     * Sets pipeline component name, this is java canonical name used 
     * to create an instance of a pipeline component
     * 
     * @param name 
     */

    public void setName(String name) 
    {
        this.name = name;
    }

    /**
     * Sets parameters required by the pipeline component to initialize
     * 
     * @param params 
     */
    public void setParams(String params)
    {
        this.params = params;
    }

    /**
     * Sets pipeline component priority, this is used to the pipeline 
     * engine decide which pipeline component should be invoked first to
     * process a request
     * 
     * @param priority 
     */
    public void setPriority(int priority) 
    {
        this.priority = priority;
    }

    /**
     * Get pipeline component canonical name
     * 
     * @return 
     */
    public String getName()
    {
        return name;
    }

    /**
     * Get parameters required by pipeline component to initialize
     * 
     * @return params
     */
    public String getParams() 
    {
        return params;
    }

    /**
     * Gets pipeline component priority
     * 
     * @return priority
     */
    public int getPriority() 
    {
        return priority;
    }

    /**
     * Gets a flag indicating whether this pipeline component is compulsory 
     * or not
     * 
     * @return <code>true</code> if the pipeline component is compulsory
     *          <br> otherwise <code>false</code>
     */
    public boolean isCompulsory() 
    {
        return compulsory;
    }

    /**
     * Gets a flag indicating whether this pipeline component is should 
     * be active or not
     * 
     * @return <code>true</code> if the pipeline component should be active
     *          <br> otherwise <code>false</code>
     */
    public boolean isActive() 
    {
        return active;
    }
    
    /**
     * Check if pipeline component configuration is valid
     * 
     * @return <code>true</code> if configuration is valid <br>
     *         otherwise <code>false</code>
     */
    public boolean isValid() 
    {
    	if(name == null || name.trim().isEmpty())
    	{
    		return false;
    	}
    	
    	return true;
    }

    @Override
    public int compareTo(Component o) 
    {
        return priority-o.getPriority();
    }
}
