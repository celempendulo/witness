/*
 * Copyright 2019 mpendulo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package celecode.ide.configuration;


/**
 * This class is used to store database connection properties
 *
 * @author mpendulo
 */
public class DBConfig
{
    private String host;
    
    private int port;
    
    private String dbname;
    
    private String username;
    
    private String password;
    
    private boolean authenticate;
    
    private boolean load_config;

    public DBConfig() 
    {
        authenticate = false;
        load_config = false;

    }

    /**
     * Sets database host, eg IP address
     * 
     * @param host 
     */
    public void setHost(String host) 
    {
        this.host = host;
    }

    /**
     * Sets connection username for authentication
     * 
     * @param username 
     */
    public void setUsername(String username) 
    {
        this.username = username;
    }

    /**
     * Sets connection password for authentication
     * 
     * @param password 
     */
    public void setPassword(String password) 
    {
        this.password = password;
    }

    /**
     * Sets a flag for authentication
     * 
     * @param authenticate 
     */
    public void setAuthenticate(boolean authenticate) 
    {
        this.authenticate = authenticate;
    }

    /**
     * Sets a flag to load configuration to database tables
     * 
     * @param load_config 
     */
    public void setLoaConfig(boolean load_config)
    {
        this.load_config = load_config;
    }

    /**
     * Sets database name
     * 
     * @param dbname 
     */
    public void setDBName(String dbname) 
    {
        this.dbname = dbname;
    }


    /**
     * Sets a database host port to be used
     * 
     * @param port 
     */
    public void setPort(int port) 
    {
        this.port = port;
    }

    /**
     * 
     * @return database host
     */
    public String getHost() 
    {
        return host;
    }

    /**
     * 
     * @return <code>true</code> if the flag to authenticate is set<br>
     *          <code>false</code> otherwise
     */
    public boolean shouldAuthenticate()
    {
        return authenticate;
    }

    /**
     * 
     * @return <code>true</code> if the flag to lad configuration to database tables is set
     *          <code>false</code> otherwise
     */
    public boolean shouldLoadConfig()
    {
        return load_config;
    }

    /**
     * 
     * @return returns database host port
     */
    public int getPort()
    {
        return port;
    }

    /**
     * 
     * 
     * @return database name
     */
    public String getDBName() 
    {
        return dbname;
    }

    public String getPassword() 
    {
        return password;
    }

    public String getUsername() 
    {
        return username;
    }
    
    

}
