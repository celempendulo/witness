/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.witness.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author mpendulo
 */
public class Contact 
{
    @JsonProperty("email")
    private String email;
    
    @JsonProperty("telephone")
    private String telephone;

    public String getEmail() 
    {
        return email;
    }

    public String getTelephone() 
    {
        return telephone;
    } 
    
}
