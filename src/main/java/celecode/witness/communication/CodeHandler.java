/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.witness.communication;

import celecode.ide.pipeline.ICommunicator;
import celecode.witness.Witness;
import com.fasterxml.jackson.databind.JsonNode;

/**
 *
 * @author mpendulo
 */
public class CodeHandler 
{

    public static void process(ICommunicator communicator, JsonNode node)
            throws Exception
    {
        System.out.println("message : "+node);
        if(node != null && node.has(Action.Keys.ACTION))
        {
            switch(node.get(Action.Keys.ACTION).textValue())
            {
                case Action.CodeActions.ADD_INPUT:
                    communicator.writeCodeInput(node.get(
                            Action.Keys.INPUT).textValue());
                    break;
                
                case Action.CodeActions.RUN:
                    Witness.getWitness().processMessage(node.findValue(
                            Action.Keys.MESSAGE).toString(), communicator);
                    break;
                    
                default:
                    System.out.println("unknown");
            }
        }
    
    }
}
