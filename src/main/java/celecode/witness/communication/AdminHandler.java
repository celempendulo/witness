package celecode.witness.communication;

import java.util.List;

import javax.websocket.Session;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import celecode.ide.configuration.Language;
import celecode.ide.util.Converter;
import celecode.witness.Witness;

public class AdminHandler 
{
	
	
	public static void process(Session session, JsonNode node) throws Exception
	{
		if(node != null && node.has(Action.Keys.ACTION))
		{
			switch (node.get(Action.Keys.ACTION).textValue())
			{
				case Action.AdminActions.INIT:
					sendInitData(session);
					break;
	
				default:
					break;
			}
		}
		
	}
	
	
	private static void sendInitData(Session session) throws Exception
	{
		List<Language> languages = Witness.getWitness().getConfiguration().getLanguages();
		ArrayNode data = new ObjectMapper().createArrayNode();
		
		languages.stream().forEach(language ->
		{
			ObjectNode node = Converter.MAPPER.createObjectNode();
			node.put(Action.Keys.NAME, language.getName());
			node.put(Action.Keys.VERSION, language.getVersion());
			node.put(Action.Keys.TEXT, language.toString());
                        node.put(Action.Keys.EXTENSION, language.getExtension());
			
			data.add(node);
		});
		
		ObjectNode message = Converter.MAPPER.createObjectNode();
		message.put(Action.Keys.DATA, data.toString());
		message.put(Action.Keys.TYPE, Action.ActionTypes.ADMIN);
		message.put(Action.Keys.ACTION, Action.AdminActions.INIT);
		

		if(session.isOpen())
		{
			session.getBasicRemote().sendText(message.toString());
		}
		
		
	}

}
