/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.witness.communication.servlets;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mpendulo
 */
public class ProcessVisitor extends HttpServlet 
{

    /**
	 * 
	 */
	private static final long serialVersionUID = -7864988789328988288L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        response.setContentType("text/plain;charset=UTF-8");
        try 
        {
            //String messageString = Converter.fromKeyValueArrayToString(request.getParameterMap());
            //Message message = Converter.fromJsonToMessage(messageString);
            // Bouncer.getBouncer().getEngine().process(message, 
            //       new ServletCommunicator(response.getOutputStream(), request.getInputStream()));
         } 
        catch (Exception ex) 
        {
            Logger.getLogger(ProcessVisitor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
{
        processRequest(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "This servlet process visitor requests";
    }
    

}
