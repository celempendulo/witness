/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.witness.communication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;


/**
 *
 * @author mpendulo
 */
public class Action 
{
    public static class ActionTypes
    {
        public static final String ADMIN = "ADMIN";
        public static final String CODE = "CODE";
    }
    
    /**
     * List of source code related action
     */
    public static class CodeActions
    {
        public static final String ADD_INPUT = "ADD INPUT";
        public static final String TERMINATE = "TERMINATE";
        public static final String RUN = "RUN";
        public static final String SHOW_ERROR = "SHOW ERROR";
        public static final String SHOW_OUTPUT = "SHOW OUTPUT";
        public static final String FIRST = "FIRST";
        public static final String  ALLOW_INPUT = "ALLOW INPUT";
        public static final String BLOCK_INPUT = "BLOCK INPUT";
    }
    
    /**
     * List of ADMIN action actions
     */
    public static class AdminActions
    {
        public static final String LOGIN = "LOGIN";
        public static final String LOGOUT = "LOGOUT";
        public static final String REGISTER = "REGISTER";
        public static final String DEREGISTER = "DEREGISTER";
        public static final String INIT = "INIT";
    }
    
    /**
     * List of generic keys 
     */
    public static class Keys
    {
        public static final String ACTION = "ACTION";
        public static final String MESSAGE = "MESSAGE";
        public static final String TYPE = "TYPE";
        public static final String CONTENT = "CONTENT";
        public static final String LANGUAGE = "LANGUAGE";
        public static final String NAME = "NAME";
        public static final String VERSION = "VERSION";
        public static final String TEXT = "TEXT";
        public static final String LANGUAGES = "LANGUAGES";
        public static final String DATA = "DATA";
        public static final String EXTENSION = "EXTENSION";
        public static final String INPUT = "INPUT";
    }
    
    /**
     * create a message to be send to the client containing code output
     * 
     * @param content line of code output
     * 
     * @param isFirst true if this is the first line of output being sent 
     * to the client for the current message being processed
     * 
     * @return 
     */
    public static String getCodeOutput(String content, boolean isFirst)
    {
        ObjectNode node = new ObjectMapper().createObjectNode();
        node.put(Keys.TYPE, ActionTypes.CODE);
        node.put(Keys.ACTION, CodeActions.SHOW_OUTPUT);
        node.put(Keys.CONTENT, content);
        node.put(CodeActions.FIRST, isFirst);
        
        return node.toString();
    }
    
    /**
     * Create and return source code related command to be send to a client,
     * 
     * e.g the command can be telling the client to allow the console to be editable
     * 
     * 
     * @param type command type
     * 
     * @param content content if required
     * 
     * @return 
     */
    public static String getCodeCommand(String type, String content)
    {
        ObjectNode node = new ObjectMapper().createObjectNode();
        node.put(Keys.ACTION, ActionTypes.CODE);
        node.put(Keys.TYPE, type);
        if(content != null)
        {
            node.put(Keys.CONTENT, content);
        }
        
        return node.toString();
    }
    
}
