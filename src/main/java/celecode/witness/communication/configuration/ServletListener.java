package celecode.witness.communication.configuration;

import celecode.ide.configuration.Configuration;
import celecode.ide.util.Converter;
import celecode.witness.Witness;
import celecode.witness.configuration.ApplicationInformation;
import celecode.witness.database.WitnessDB;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServletListener implements ServletContextListener
{

	@Override
	public void contextDestroyed(ServletContextEvent event) 
	{
		
	}

	@Override
	public void contextInitialized(ServletContextEvent event) 
	{
            boolean ready = false;
            String message = "FAILED TO INITALIZE";
            try 
            {
                Path applicationConfig = Paths.get(event.getServletContext().
                        getRealPath("WEB-INF/configuration/application.json"));
                
                if(Files.exists(applicationConfig))
                {
                    Witness witness = Witness.getInstance(new String(Files.readAllBytes(applicationConfig)));
                    ApplicationInformation information = witness.getApplicationInformation();
                    
                    if(information.getDBConfig().shouldLoadConfig())
                    {
                        loadConfiguration(witness.getDatabase(), event);
                    }
                    witness.initialise();
                    ready = true;
                }
                else
                {
                    message = "APPLICATION INFORMATION FILE NOT FOUND";
                }
            } 
            catch (Exception ex) 
            {
                Logger.getLogger(ServletListener.class.getName()).log(Level.SEVERE, null, ex);
                message = ex.getMessage();
            }

            if(!ready)
            {
                throw new RuntimeException("<message> : "+message);
            }
	}
        
        private static void loadConfiguration(WitnessDB database, ServletContextEvent event) throws Exception
        {
            Path configurationPath = Paths.get(event.getServletContext().
                        getRealPath("WEB-INF/configuration/configuration.json"));
            
            if(Files.exists(configurationPath))
            {
                Configuration config = (Configuration) Converter.fromJsonToObject(new String(Files.
                        readAllBytes(configurationPath)), Configuration.class);
                database.loadConfiguration(config);
            }
            else
            {
                throw new RuntimeException("<message> : Configuration file not found in "+configurationPath.toString());
            }
            
        }

}
