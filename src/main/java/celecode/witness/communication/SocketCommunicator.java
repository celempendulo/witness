/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.witness.communication;

import celecode.ide.pipeline.ICommunicator;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.Session;

/**
 *
 * @author mpendulo
 */
public class SocketCommunicator implements ICommunicator
{

    private final Session session;
    private boolean firstCodeOutput;
    private Writer writer;
    private boolean isRunning;
    private final Queue<String> waitingInput;
    
    public SocketCommunicator(Session session) 
    {
        this.session = session;
        firstCodeOutput = true;
        isRunning = false;
        waitingInput = new LinkedList<>();
    }
    
    public OutputStream getOutputStream() 
    {
        try 
        {
            return session.getBasicRemote().getSendStream();
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(SocketCommunicator.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    @Override
    public void sendConsoleOutput(String message) 
    {
        try 
        {
            message = Action.getCodeOutput(message, firstCodeOutput);
            session.getBasicRemote().sendText(message);
            if(firstCodeOutput)
            {
                firstCodeOutput = false;
            }
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(SocketCommunicator.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void setOutputStream(OutputStream outputStream) 
    {
        try
        {
            writer = new OutputStreamWriter(outputStream, "UTF-8");
            writer.flush();
        }
        catch(IOException ex)
        {
        }
    }

    @Override
    public void writeCodeInput(String data) 
    {
        if(!isRunning)
        {
            waitingInput.add(data);
            return;
        }
        
        try 
        {
            writer.write(data);
            writer.flush();
        }  
       catch (IOException ex) 
        {
            Logger.getLogger(SocketCommunicator.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
    }

    protected void sendCodeCommand(String type, String content) 
    {
        try 
        {
            String message = Action.getCodeCommand(type, content);
            session.getBasicRemote().sendText(message);
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(SocketCommunicator.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void disableInteraction() 
    {
        firstCodeOutput = true;
        isRunning = false;
    }
    
    @Override
    public void enableInteraction()
    {
        while(!waitingInput.isEmpty() && writer != null)
        {
            try 
            {
                writer.write(waitingInput.poll());
                writer.flush();
            } 
            catch (IOException ex)
            {
                Logger.getLogger(SocketCommunicator.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
        }
        
        isRunning = true;
    }
    
    
    
}
