/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.witness.communication;

import celecode.ide.pipeline.ICommunicator;
import celecode.ide.util.Converter;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.fasterxml.jackson.databind.JsonNode;

/**
 *
 * @author mpendulo
 */
@ServerEndpoint("/endpoint")
public class WebSocket
{
    private static Map<String, ICommunicator> clients;

    public WebSocket() 
    {
        clients = new TreeMap<>();
    }
    

    @OnOpen
    public void open(Session session)
    {
        clients.put(session.getId(), new SocketCommunicator(session));
    }
    
    @OnMessage
    public void message(Session session, String message) 
    {
        try 
        {
            JsonNode node = Converter.fromStringToObjectNode(message);
            if(node.has(Action.Keys.TYPE))
            {
               String action = node.get(Action.Keys.TYPE).textValue();
               if(null != action)
               {
                    switch (action) 
                    {
                        case Action.ActionTypes.ADMIN:
                        	AdminHandler.process(session, node);
                            break;
                        case Action.ActionTypes.CODE:
                                processCodeRequest(session, node);
                            break;
                    }
               }
            }
            else
            {
            }
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(WebSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @OnClose
    public void onClose(Session session)
    {
        clients.remove(session.getId());
    }
    
    public void onError(Session session, Throwable trowable)
    {
        
    }
    
    /***
     * process code request from the client,
     * e.g the client may be requesting to compile source code or execute source code
     * 
     * @param session
     * @param node 
     */
    public void processCodeRequest(Session session, JsonNode node) 
    {
        
    	try
    	{
            CodeHandler.process(clients.get(session.getId()), node);
        } 
        catch (Exception ex) 
        {
            Logger.getLogger(WebSocket.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
    }
    
    
}
