/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.witness.sourcecode.testdata;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mpendulo
 */
public class FileData extends AData
{
    private final String path;
    
    public FileData(String path) 
    {
        super(Type.FILE_READ);
        this.path = path;
    }

    public String getPath()
    {
        return path;
    }
    
    public void writeData(String filename, String data)
    {
        try 
        {
            Files.write(Paths.get(filename), data.getBytes());
        } 
        catch (IOException ex)
        {
            Logger.getLogger(FileData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
