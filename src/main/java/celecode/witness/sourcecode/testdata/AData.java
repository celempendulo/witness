/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.witness.sourcecode.testdata;

import java.util.LinkedList;

/**
 *
 * @author mpendulo
 */
public abstract class AData 
{
    private final String type;
    private final static LinkedList<String> types = new LinkedList<>();

    public AData(String type)
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    
    
    public static class Type
    {
        public static final String FILE_READ = "FR";
        public static final String IO = "IO";
        public static final String PARAMETER_ON_EXECUTE = "POE";
    }
    
    static
    {
        types.add(Type.FILE_READ);
        types.add(Type.IO);
        types.add(Type.PARAMETER_ON_EXECUTE);
    }

    public static LinkedList<String> getTypes()
    {
       return types;
    }
    
    
}
