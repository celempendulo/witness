/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.witness.sourcecode;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author mpendulo
 */
public class Question
{
    @JsonProperty("_id")
    protected String id;
    
    @JsonProperty("name")
    protected String name;
    
    @JsonProperty("description")
    private String description;
    
    @JsonProperty("examples")
    protected List<Example> examples;

    public Question(String id, String name, List<Example> examples) 
    {
        this.id = id;
        this.name = name;
        this.examples = examples;
    }

    public Question() 
    {
        examples = new LinkedList<>();
    }
    
    public void setExamples(List<Example> examples) 
    {
        this.examples = examples;
    }

    public List<Example> getExamples() 
    {
        return examples;
    }

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }

    public String getDescription() 
    {
        return description;
    }

    public void setDescription(String description) 
    {
        this.description = description;
    }
    
    
    
    public static class Example
    {
        @JsonProperty("input")
        private String input;
        
        @JsonProperty("output")
        private String output;

        public Example() 
        {
        }

        public String getInput() 
        {
            return input;
        }

        public void setInput(String input)
        {
            this.input = input;
        }

        public String getOutput() 
        {
            return output;
        }

        public void setOutput(String output) 
        {
            this.output = output;
        }  
        
    }
    
    
}
