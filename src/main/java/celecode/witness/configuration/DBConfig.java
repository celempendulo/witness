/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.witness.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 *
 * @author mpendulo
 */
public class DBConfig
{
    @JsonProperty("host")
    private String host;
    
    @JsonProperty("port")
    private int port;
    
    @JsonProperty("dbname")
    private String dbname;
    
    @JsonProperty("username")
    private String username;
    
    @JsonProperty("password")
    private String password;
    
    @JsonProperty("authenticate")
    private boolean authenticate;
    
    @JsonProperty("load_config")
    private boolean load_config;

    public DBConfig() 
    {
        authenticate = false;
        load_config = false;

    }

    /**
     * Sets database host, eg IP address
     * 
     * @param host 
     */
    public void setHost(String host) 
    {
        this.host = host;
    }

    /**
     * Sets connection username for authentication
     * 
     * @param username 
     */
    public void setUsername(String username) 
    {
        this.username = username;
    }

    /**
     * Sets connection password for authentication
     * 
     * @param password 
     */
    public void setPassword(String password) 
    {
        this.password = password;
    }

    /**
     * Sets a flag for authentication
     * 
     * @param authenticate 
     */
    public void setAuthenticate(boolean authenticate) 
    {
        this.authenticate = authenticate;
    }

    /**
     * Sets a flag to load configuration to database tables
     * 
     * @param load_config 
     */
    public void setLoaConfig(boolean load_config)
    {
        this.load_config = load_config;
    }

    /**
     * Sets database name
     * 
     * @param dbname 
     */
    public void setDBName(String dbname) 
    {
        this.dbname = dbname;
    }


    /**
     * Sets a database host port to be used
     * 
     * @param port 
     */
    public void setPort(int port) 
    {
        this.port = port;
    }

    /**
     * 
     * @return database host
     */
    public String getHost() 
    {
        return host;
    }

    /**
     * 
     * @return <code>true</code> if the flag to authenticate is set<br>
     *          <code>false</code> otherwise
     */
    public boolean shouldAuthenticate()
    {
        return authenticate;
    }

    /**
     * 
     * @return <code>true</code> if the flag to lad configuration to database tables is set
     *          <code>false</code> otherwise
     */
    public boolean shouldLoadConfig()
    {
        return load_config;
    }

    /**
     * 
     * @return returns database host port
     */
    public int getPort()
    {
        return port;
    }

    /**
     * 
     * 
     * @return database name
     */
    public String getDBName() 
    {
        return dbname;
    }

    public String getPassword() 
    {
        return password;
    }

    public String getUsername() 
    {
        return username;
    }
    
    

}
