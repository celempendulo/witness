/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.witness.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author mpendulo
 */

public class ApplicationInformation 
{
    @JsonProperty("DBConfig")
    private DBConfig dbConfig;

    public ApplicationInformation() 
    {
        
    }

    public void setDBConfig(DBConfig dbConfig) 
    {
        this.dbConfig = dbConfig;
    }

    public DBConfig getDBConfig() 
    {
        return dbConfig;
    }
    
}
