/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.witness.database;

import celecode.ide.configuration.Configuration;
import celecode.ide.configuration.Component;
import celecode.ide.configuration.Language;
import celecode.witness.configuration.DBConfig;
import celecode.witness.entities.Organisation;
import celecode.witness.sourcecode.Question;
import celecode.ide.util.Converter;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.UpdateResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.bson.Document;

/**
 *
 * @author mpendulo
 */
public class WitnessDB 
{

    private final MongoDatabase database;
    MongoClient client;

   
    public WitnessDB(DBConfig dbConfig)throws Exception
    {
        validate(dbConfig);
        
        if(dbConfig.shouldAuthenticate())
        {
            MongoCredential credential = MongoCredential.createCredential(dbConfig.getUsername(), dbConfig.getDBName(),
                    dbConfig.getPassword().toCharArray());
            client = new MongoClient(Arrays.asList(new ServerAddress(dbConfig.getHost(), dbConfig.getPort())),
                    credential, MongoClientOptions.builder().build());
        }
        else
        {
           client = new MongoClient(dbConfig.getHost(), dbConfig.getPort());
        }
        database = client.getDatabase(dbConfig.getDBName());
    }
    
    
    private void validate(DBConfig config) throws Exception
    {
        if(config.getHost() == null)
        {
            throw new Exception("HOST CANNOT BE NULL");
        }
        
        if(config.getDBName() == null)
        {
            throw new Exception("DATABASE NAME CANNOT NULL NULL");
        }
        
        if(config.shouldAuthenticate())
        {
            if(config.getUsername() == null)
            {
                throw new Exception("AUTHENTICATION ENABLED : USERNAME CANNOT BE NULL");
            }
            
            if(config.getPassword() == null)
            {
                throw new Exception("AUTHENTICATION ENABLED : PASSWORD CANNOT BE NULL");
            }
        }
        
    }
    
    public void loadConfiguration(Configuration configuration) throws Exception
    {
        if(configuration == null)
        {
            throw new Exception("CANNOT LOAD NULL CONGIGURATION");
        }
        
        loadComponents(configuration.getComponents());
        loadLanguages(configuration.getLanguages());
    }
    
    public void loadComponents(List<Component> components) throws Exception
    {
        if(components == null || components.isEmpty())
        {
            throw new Exception("1 OR MORE COMPONENTS MUST BE CONFIGURED");
        }
        
        MongoCollection<Document> collection = database.getCollection(CollectionNames.COMPONENTS);
        collection.deleteMany(new BasicDBObject());
        List<Document> componentDocuments = new LinkedList<>();
        
        for(Component component :components)
        {
           componentDocuments.add(Document.parse(Converter.fromObjectJson(component)));
        }
        collection.deleteMany(new BasicDBObject());
        collection.insertMany(componentDocuments);
    }
    
    
    public void loadLanguages(List<Language> languages) throws Exception
    {
        if(languages == null || languages.isEmpty())
        {
            throw new Exception("1 OR MORE LANGUAGES MUST BE CONFIGURED");
        }
        MongoCollection<Document> collection = database.getCollection(CollectionNames.LANGUAGES);
        collection.deleteMany(new BasicDBObject());
        List<Document> languageDocuments = new LinkedList<>();
        
        for(Language language :languages)
        {
           languageDocuments.add(Document.parse(Converter.fromObjectJson(language)));
        }
        collection.deleteMany(new BasicDBObject());
        collection.insertMany(languageDocuments);
        
    }
    
    public List<Language> getLanguages() throws Exception
    {
        MongoCollection<Document> collection = database.getCollection(CollectionNames.LANGUAGES);
        FindIterable<Document> documents = collection.find(new BasicDBObject());
        List<Language> languages = new LinkedList<>();
        
        for(Document document : documents)
        {
            document.remove("_id");
            languages.add((Language) Converter.fromJsonToObject(document.toJson(), Language.class));
        }
        return languages;
    }
    
    public void loadConfigurationFromFile(String pathStr) throws Exception
    {
        Path path = Paths.get(pathStr);
        Configuration config = (Configuration) Converter.fromJsonToObject(new String(Files.readAllBytes(path)),
                Configuration.class);
        loadConfiguration(config);
    }
    
    public List<Component> getComponents() throws Exception
    {
        MongoCollection<Document> collection = database.getCollection(CollectionNames.COMPONENTS);
        FindIterable<Document> documents = collection.find(new BasicDBObject());
        List<Component> components = new LinkedList<>();
        
        for(Document document : documents)
        {
            document.remove("_id");
            components.add((Component) Converter.fromJsonToObject(document.toJson(), Component.class));
        }
        
        return components;
    }
    
    public boolean addQuestion(String organisationId, Question question) throws Exception
    {
        MongoCollection<Document> colleaction = database.getCollection(CollectionNames.QUESTIONS);
        Document document = Document.parse(Converter.fromObjectJson(question));
        UpdateResult result = colleaction.updateOne(new BasicDBObject("_id", organisationId),
                Updates.push("questions", document));
       
        return result.wasAcknowledged();
    }
    
    public void addOrganisation(Organisation organisation) throws Exception
    {
         MongoCollection<Document> colleaction = database.getCollection(CollectionNames.ORGANISATION);
         Document document = Document.parse(Converter.fromObjectJson(organisation));
         colleaction.insertOne(document);
    }
    
    public void close()
    {
    	client.close();
    }
    
    public static class CollectionNames
    {
        public static final String LANGUAGES = "languages";
        public static final String COMPONENTS = "components";
        public static final String QUESTIONS = "questions";
        public static final String ORGANISATION = "organisation";
    }
    
}
