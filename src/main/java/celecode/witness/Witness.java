/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package celecode.witness;

import celecode.witness.configuration.ApplicationInformation;
import celecode.ide.configuration.Configuration;
import celecode.ide.configuration.Component;
import celecode.ide.configuration.Language;
import celecode.witness.database.WitnessDB;
import celecode.ide.pipeline.Engine;
import celecode.ide.pipeline.ICommunicator;
import celecode.ide.util.Converter;
import java.util.List;

/**
 *
 * @author mpendulo
 */
public class Witness 
{

    private static Witness witness;
    private final Engine engine;
    private final WitnessDB database;
    private Configuration configuration;
    private final ApplicationInformation information;
    
    /**
     * Creates an instance of type Witness
     * 
     * @param information application information used to connect to database
     * 
     * @throws Exception when database connection information is invalid
     */
    private Witness(ApplicationInformation information) throws Exception
    {
        this.information = information;
        this.database = new WitnessDB(information.getDBConfig());
        this.engine = new Engine();
    }
    
    /**
     * 
     * @throws Exception 
     */
    public void initialise() throws Exception
    {
       initializeCongiguration();
       engine.init(configuration);
    }
    
    /***
     * Initialize configuration with data from the database
     * 
     * @throws Exception 
     */
    private void initializeCongiguration() throws Exception
    {
        List<Component> components = database.getComponents();
        List<Language> languages = database.getLanguages();
        
        configuration = new Configuration();
        configuration.setComponents(components);
        configuration.setLanguages(languages);
    }
    
    /**
     * 
     * 
     * @param message encoded witness code message in a string  
     * 
     * @param communicator entity used to communicate with the client
     * 
     * @throws Exception when an error occurred during message processing
     */
    public void processMessage(String message, ICommunicator communicator)
            throws Exception
    {
        if(!engine.isInitialised())
        {
            throw new Exception("APPLICATION DID NOT INITIALSISE, "
                    + "HENCE CENNOT PROCESS A MESSAGE");
        }
        engine.process(Converter.fromJsonToMessage(message), communicator);
    }
    

    /**
     * Return instance if Witness 
     * 
     * @return instance of witness if created otherwise will returnj <code>null</code>
     */
    public static Witness getWitness() 
    {
        return witness;
    }

    /**
     * 
     * @return configuration
     */
    public Configuration getConfiguration() 
    {
        return configuration;
    }

    /**
     * @return witness database
     */
    public WitnessDB getDatabase()
    {
        return database;
    }

    /**
     * 
     * @return engine 
     */
    public Engine getEngine() 
    {
        return engine;
    }

    /**
     * 
     * @return application information object
     */
    public ApplicationInformation getApplicationInformation() 
    {
        return information;
    }
    
    
    
    
    /***
     * Create and return instance of witness if not created yet,
     * Should be invoked once (on start-up)
     * 
     * @param applicationInformation JSON application information encoded in a string
     * 
     * @return Witness instance
     * 
     * @throws Exception 
     */
    public static Witness getInstance(String applicationInformation) throws Exception
    {
        if(witness == null)
        {
            ApplicationInformation information = (ApplicationInformation) Converter.
                    fromJsonToObject(applicationInformation, ApplicationInformation.class);
            witness = new Witness(information);
        }
        else
        {
            throw  new Exception("WITNESS ALREADY EXIST USE getInstance()");
        }
        return witness;
    }
   
}
