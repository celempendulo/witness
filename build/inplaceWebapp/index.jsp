<%-- 
    Document   : index
    Created on : 13 Dec 2018, 3:25:49 AM
    Author     : mpendulo
--%>

<%@page import="java.util.List"%>
<%@page import="celecode.ide.configuration.Language"%>
<%@page import="celecode.ide.configuration.Configuration"%>
<%@page import="celecode.witness.Witness"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
    <link href="resources/static/style/bootstrap.min.css" rel="stylesheet">
    <link href="resources/static/style/style.css" rel="stylesheet">
    <link href="resources/static/style/home.css" rel="stylesheet">
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a style="padding-top:5px; font-weight:bold" class="navbar-brand" href="#">
                <span><img alt="Brand" src="resources/static/img/logo.png"></span>
                Bouncer
                </a>
            </div>

            <ul class="nav navbar-nav navbar-right" style="padding-left:10px;">
                <li style="padding-right:5px;"><a  class="btn btn-default" href="#"> <b>Sign In</b></a></li>
                <li style="padding-right:30px;"><a class="btn btn-default" href="#" ><b>Sign Up</b></a></li>
            </ul>
        </div>
    </nav>

    <div class="contaniner">
        <div class="row">
            <div class="col-md-1 col-lg-2 side-panel">
                <form>
                    
                    <div class="form-group">
                        <select id="languages" class="form-control">
                            <c:forEach items="${languages}" var="language">
                                <option value="${language.getName()}" 
                                        version="${language.getVersion()}"
                                        extension="${language.getExtension()}">
                                    ${language.toString()}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <input id="filename" type="text" placeholder="Filename" class="form-control">
                    </div>
                    
                </form>
            </div>

            <div  id="editor" class="col-md-10 col-lg-8">
            </div>
            
            <div class="col-md-1 col-lg-2 side-panel">
                <form>
                    <input class="form-control side-button" type="button" value="Run" onclick="sendMessage()">
                    <input class="form-control side-button" type="button" value="Export">
                    <input class="form-control side-button" type="button" value="Import" onclick="loadFile()">
                </form>
            </div>
        </div>
        
        <div class="row">
            <div id="input" class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                <span>Input argument</span>
                <input type="text" style="width: 100%;">
            </div>

            <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
                <span>Output Console</span>
                <textarea id="console" style="width: 100%; height:70px;" readonly>
                </textarea>
            </div>
        </div>
    </div>

    <script src="resources/static/script/jquery-3.3.1.min.js" ></script>
    <script src="resources/static/script/bootstrap.min.js"></script>
    <script src="resources/static/script/angular.min.js"></script>
    <script src="resources/static/script/ace.js"></script>
    <script src="resources/static/script/mode-java.js"></script>
    <script src="resources/static/script/code-manager.js"></script>

    <script>
        var editor = ace.edit("editor");
        editor.session.setMode("ace/mode/java");
        var socket = new WebSocket("ws://localhost:8080/endpoint");
        socket.onmessage = function(message)
            {
                var data =  JSON.parse(message.data);
                
                if(!(data["ACTION"] === undefined))
                {
                    switch (data["ACTION"]) 
                    {
                        case "CODE":
                              handleCode(data);
                            break;

                        default:
                              console.log("UNKNOWN MESSAGE : "+JSON.stringify(data));
                            break;
                    }
                }
                else
                {
                    console.log("ACTION IS MISSING : "+JSON.stringify(data));
                }
            };
      
        function sendMessage()
        {
            var language = $('#languages').find(":selected");
            var name = language.val();
            var version = language.attr("version");
            var extension = language.attr("extension");
            var sourceCode = editor.getValue();
            var filename = $("#filename").val().trim();
            
            if(filename.length === 0)
            {
                alert("FILENAME CANNOT BE EMPTY");
            }
            else
            {
                            
                var message = {
                                "FILENAME":filename, 
                                "FILE EXTENSION":extension, 
                                "LANGUAGE VERSION":version, 
                                "LANGUAGE NAME":name, 
                                "ACTION":"1", 
                                "SOURCE CODE":sourceCode
                                };
                var action = {"ACTION":"CODE", "MESSAGE":message};
                socket.send(JSON.stringify(action));
            }
        }
    </script>
</body>
</html>

